//
//  PostDetailViewController.swift
//  Exam 31Digital
//
//  Created by Admin on 27/8/18.
//  Copyright © 2018 peterencio. All rights reserved.
//

import UIKit

class PostDetailViewController: UIViewController {

    @IBOutlet weak var label_title: UILabel!
    @IBOutlet weak var label_details: UILabel!
    
    var post: Post?
    override func viewDidLoad() {
        super.viewDidLoad()
        guard  let post = self.post else {
            return
        }
        label_title.text = post.title
        label_details.text = post.body

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  PostTableViewCell.swift
//  Exam 31Digital
//
//  Created by Admin on 27/8/18.
//  Copyright © 2018 peterencio. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {
    static let CELL_IDENTIFIER = "postCell"
    
    @IBOutlet weak var label_title: UILabel!
    @IBOutlet weak var label_user: UILabel!
    
    var post: Post?{
        didSet{
            guard let post = post else {
                return
            }
            label_title.text = post.title
            label_user.text = "by: " + String(post.userId)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

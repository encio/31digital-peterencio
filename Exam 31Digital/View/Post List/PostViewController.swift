//
//  ViewController.swift
//  Exam 31Digital
//
//  Created by Admin on 27/8/18.
//  Copyright © 2018 peterencio. All rights reserved.
//

import UIKit

class PostViewController: UIViewController {
    let networkManager = NetworkManager()
    private let refreshControl = UIRefreshControl()
    var posts = [Post]()
    var selectedPost: Post?
    @IBOutlet weak var tableView_posts: UITableView!{
        didSet{
            tableView_posts.delegate = self
            tableView_posts.dataSource = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        getPost()
        
        //pull to refresh
        if #available(iOS 10.0, *) {
            tableView_posts.refreshControl = refreshControl
        } else {
            tableView_posts.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshPost(_:)), for: UIControlEvents.valueChanged)
    }
    private func getPost(){
        showAlertInternetAlert()
        networkManager.getPosts { (retrievedPosts) in
            self.posts.removeAll()
            for post in retrievedPosts{
                self.posts.append(post)
            }
            DispatchQueue.main.async {
                self.tableView_posts.reloadData()
                self.setTitle()
                self.refreshControl.endRefreshing()
            }
        }
    }
    @objc private func refreshPost(_ sender: Any) {
        getPost()
        setTitle()
    }
    private func setTitle(){
        self.title = "POSTS (" + String(self.posts.count) + ")"
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == POSTVIEWCONTROLLER_TO_DETAILSVIEWCONTROLLER {
            guard let detailsViewController = segue.destination as? PostDetailViewController else{
                return
            }
            guard let post = selectedPost else {
                return
            }
            detailsViewController.post = post
        }
    }
    func showAlertInternetAlert() {
        if !networkManager.isInternetAvailable() {
            self.refreshControl.endRefreshing()
            let alert = UIAlertController(title: "You are offline", message: "Please enable internet", preferredStyle: .alert)
            let action = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func addPost(_ sender: UIBarButtonItem) {
        showAlertInternetAlert()
        let alertController = UIAlertController(title: "Your ID is 31", message: "Add new post", preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Title of the post"
        }
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Message"
        }
        alertController.addAction(UIAlertAction(title: "POST", style: .default, handler: { (action) in
            let title = alertController.textFields![0] as UITextField
            let message = alertController.textFields![1] as UITextField
            guard let titleText = title.text else {
                return
            }
            guard !titleText.isEmpty else {
                return
            }
            self.networkManager.newPost(title: titleText, message: message.text ?? "No Message", userID: 31){ post in
                guard let post = post else {
                    return
                }
                DispatchQueue.main.async {
                    self.posts.append(post)
                    self.tableView_posts.reloadData()
                    self.setTitle()
                }
         
            }
        }))

        self.present(alertController, animated: true, completion: nil)
  
    }
    

}


extension PostViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PostTableViewCell.CELL_IDENTIFIER, for: indexPath) as! PostTableViewCell
        cell.post = posts[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedPost = posts[indexPath.row]
        self.performSegue(withIdentifier: POSTVIEWCONTROLLER_TO_DETAILSVIEWCONTROLLER, sender: self)
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        showAlertInternetAlert()
        if editingStyle == .delete {
            networkManager.deletePost(index: indexPath.row, completion: { (success, message) in
              
                let alertController = UIAlertController(title: "", message: message, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                DispatchQueue.main.async {
                    self.present(alertController, animated: true){
                        if success{
                            self.posts.remove(at: indexPath.row)
                            tableView.deleteRows(at: [indexPath], with: .fade)
                            self.setTitle()
                        }
                    }
                }
            })
        
        }
    }
    
}


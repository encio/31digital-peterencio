//
//  NetworkManager.swift
//  Exam 31Digital
//
//  Created by Admin on 27/8/18.
//  Copyright © 2018 peterencio. All rights reserved.
//

import Foundation
import SystemConfiguration

class NetworkManager {
    // set up the session
    let config = URLSessionConfiguration.default
    let session: URLSession!
    
    init() {
        self.session = URLSession(configuration: config)
    }
    
    
    /// List all the posts
    ///
    /// - Parameter completion: posts that is retreived
    func getPosts(completion: @escaping (_ posts: [Post])-> Void) {
        guard let url = URL(string: baseURL) else {
            print("Error: cannot create URL")
            return
        }
        let urlRequest = URLRequest(url: url)
 
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in

            guard error == nil else {
                return
            }
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                guard let posts = try JSONSerialization.jsonObject(with: responseData, options: []) as? [[String: AnyObject]] else {
                    return
                }
                var retrievedPosts = [Post]()
                for post in posts{
                    let userId = post[USER_ID] as! Int
                    let id = post[ID] as! Int
                    let title = post[TITLE] as! String
                    let body = post[BODY] as! String
                    retrievedPosts.append(Post(userId: userId, id: id, title: title, body: body))
                }
                completion(retrievedPosts)
            } catch  {
                return
            }
        }
        
        task.resume()
    }
    
    /// Deletes post
    ///
    /// - Parameters:
    ///   - index: post to deleted
    ///   - completion: if sucessfull
    func deletePost(index: Int, completion : @escaping (_ success: Bool, _ message: String) ->Void){
        let indexToDelete = String(index)
        let indexToDeleteEndPoint: String = baseURL + indexToDelete
        var deleteRequestURL = URLRequest(url: URL(string: indexToDeleteEndPoint)!)
        deleteRequestURL.httpMethod = "DELETE"
        
        
        let task = session.dataTask(with: deleteRequestURL) {
            (data, response, error) in
            guard let _ = data else {
                completion(false, error?.localizedDescription ?? "UNKNOWN ERROR")
                return
            }
            completion(true, "Succesfully Deleted!")
        }
        task.resume()
    }
    
    /// Add new post
    ///
    /// - Parameters:
    ///   - title: Title of the post
    ///   - message: body of the post
    ///   - userID: Id of the user
    ///   - completion: if success
    func newPost(title: String, message: String, userID: Int, completion : @escaping (_ post: Post?) ->()){
        guard let postUrl = URL(string: baseURL) else {
            print("Error: cannot create URL")

            return
        }
        var postUrlRequest = URLRequest(url: postUrl)
        postUrlRequest.httpMethod = "POST"
        let newPost: [String: Any] = ["title": title, "body": message, "userId": userID]
        let jsonPost: Data
        do {
            jsonPost = try JSONSerialization.data(withJSONObject: newPost, options: [])
            postUrlRequest.httpBody = jsonPost
        } catch {
            print("Error: cannot create JSON from todo")
            return
        }
        
        let task = session.dataTask(with: postUrlRequest) {
            (data, response, error) in
            guard error == nil else {
                completion(nil)
                return
            }
            guard let responseData = data else {
                completion(nil)
                return
            }
            do {
                guard let post = try JSONSerialization.jsonObject(with: responseData,
                                                                          options: []) as? [String: Any] else {
                                                                            print("Could not get JSON from responseData as dictionary")
                                                                            return
                }
                let postId = post[ID] as! Int
                completion(Post(userId: userID, id: postId, title: title, body: message))
            } catch  {
                completion(nil)
                return
            }
        }
        task.resume()
    }
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
}

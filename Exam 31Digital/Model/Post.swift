//
//  Post.swift
//  Exam 31Digital
//
//  Created by Admin on 27/8/18.
//  Copyright © 2018 peterencio. All rights reserved.
//

import Foundation

struct Post {
    var userId: Int
    var id: Int
    var title: String
    var body: String
 }
